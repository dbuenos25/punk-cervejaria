//
//  DataModel.swift
//  PunkCervejaria
//
//  Created by Diego Bueno on 15/07/21.
//  Copyright © 2021 Diego Bueno. All rights reserved.
//

import Foundation

struct BeerDetails: Codable {
    let id: Int?
    let name: String?
    let tagline: String?
    let first_brewed: String?
    let description: String?
    let image_url: String?
    let abv: Double?
    let ibu: Double?
    let target_fg: Double?
    let target_og: Double?
    let ebc: Int?
    let srm: Double?
    let ph: Double?
    let attenuation_level: Double?
    let volume: Volume?
    let boil_volume: BoilVolume?
    let method: Method?
    let ingredients: Ingredients?
    let food_pairing: [String]?
    let brewers_tips: String?
    let contributed_by: String?
}
struct Volume: Codable {
    let value: Int?
    let unit: String?
}
struct BoilVolume: Codable {
    let value: Double?
    let unit: String?
}
struct Method: Codable {
    let mash_temp: [MashTemp]?
    let fermentation: Fermentation?
}
struct MashTemp: Codable {
    let temp: Temp?
    let duration: Int?
}
struct Temp: Codable {
    let value: Double?
    let unit: String?
}
struct Ingredients: Codable {
    let malt: [Malt]?
    let hops: [Hops]?
    let yeast: String?
}
struct Hops: Codable {
    let name: String?
    let amount: Amount?
    let add: String?
    let attribute: String?
}
struct Malt: Codable {
    let name: String?
    let amount: Amount?
}
struct Amount: Codable {
    let value: Double?
    let unit: String?
}
struct Fermentation: Codable {
    let temp: Temp?
    let twist: String?
    
}


