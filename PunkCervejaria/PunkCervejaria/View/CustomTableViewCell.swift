//
//  CustomTableViewCell.swift
//  PunkCervejaria
//
//  Created by Diego Bueno on 16/07/21.
//  Copyright © 2021 Diego Bueno. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {
    
    static let identifier = "CustomTableViewCell"

    let myImageView = UIImageView()
    let labelName = UILabel()
    let labelTeor = UILabel()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        addSubview(myImageView)
        addSubview(labelName)
        addSubview(labelTeor)
        
        configureImageView()
        configureLabelName()
        configureLabelTeor()
        setConstraintsImageView()
        setConstraintsLabelName()
        setConstraintsLabelTeor()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func set(beer: BeerDetails, viewModel: ViewModel) {
        
        if let strImage = beer.image_url {
            myImageView.image = viewModel.getImage(strImage: strImage)
        }
        
        var text = ""
        if let name = beer.name{
            text += name
        }
        labelName.text = text
        
        text = "Alcohol content: "
        if let abv = beer.abv {
            text += "\(abv)"
        }
        labelTeor.text = text
        
    }
    func configureImageView(){
        myImageView.contentMode = .scaleAspectFit
        myImageView.clipsToBounds = true

    }
    func configureLabelName(){
        labelName.numberOfLines = 0
        labelName.adjustsFontSizeToFitWidth = true
        labelName.textColor = .black
        labelName.font = .systemFont(ofSize: 24, weight: .bold)
        
    }
    func configureLabelTeor(){
        labelTeor.numberOfLines = 0
        labelTeor.adjustsFontSizeToFitWidth = true
        labelTeor.textColor = .black
        labelTeor.font = .systemFont(ofSize: 13, weight: .medium)
    }
    func setConstraintsImageView(){
        myImageView.translatesAutoresizingMaskIntoConstraints = false
        myImageView.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        myImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 30).isActive = true
        myImageView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        myImageView.widthAnchor.constraint(equalTo: myImageView.heightAnchor, multiplier: 9/16).isActive = true
    }
    func setConstraintsLabelName(){
        labelName.translatesAutoresizingMaskIntoConstraints = false
        labelName.centerYAnchor.constraint(equalTo: centerYAnchor, constant: -20).isActive = true
        labelName.leadingAnchor.constraint(equalTo: myImageView.trailingAnchor, constant: 25).isActive = true
        labelName.heightAnchor.constraint(equalToConstant: 40).isActive = true
        labelName.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -12).isActive = true
    }
    func setConstraintsLabelTeor(){
        labelTeor.translatesAutoresizingMaskIntoConstraints = false
        labelTeor.centerYAnchor.constraint(equalTo: centerYAnchor, constant: 15).isActive = true
        labelTeor.leadingAnchor.constraint(equalTo: myImageView.trailingAnchor, constant: 25).isActive = true
        labelTeor.heightAnchor.constraint(equalToConstant: 30).isActive = true
        labelTeor.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -12).isActive = true
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }


}
