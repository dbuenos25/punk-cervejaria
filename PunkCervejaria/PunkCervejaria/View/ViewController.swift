//
//  ViewController.swift
//  PunkCervejaria
//
//  Created by Diego Bueno on 15/07/21.
//  Copyright © 2021 Diego Bueno. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    let tableView = UITableView(frame: .zero, style: .plain)
    let viewModel: ViewModel
    let apiURL = URL(string: "https://api.punkapi.com/v2/beers")

    var beers: [BeerDetails] = []
    
    let coordinator: MainCoordinator
    
    init(coordinator: MainCoordinator, viewModel: ViewModel) {
        self.viewModel = viewModel
        self.coordinator = coordinator
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configUI()        
        requestData()
    }
    
    func requestData(){
        if let url = apiURL {
            viewModel.fetchBeers(url: url) { (list) in
                self.beers = list
                DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
            }
        }
    }
    
    func configUI(){
        view.addSubview(tableView)
        tableView.register(CustomTableViewCell.self, forCellReuseIdentifier: CustomTableViewCell.identifier)
        tableView.frame = view.bounds
        tableView.rowHeight = 120
        tableView.dataSource = self
        tableView.delegate = self
        title = "Beers Punk"
        
    }
}
extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.beers.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CustomTableViewCell.identifier) as! CustomTableViewCell
        
        let beer = self.beers[indexPath.row]
        cell.set(beer: beer, viewModel: viewModel)
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let beer: BeerDetails = self.beers[indexPath.row]
        coordinator.beerDetail(beer: beer, viewModel: viewModel)
        
    }
}

