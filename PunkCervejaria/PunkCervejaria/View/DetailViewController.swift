//
//  DetailViewController.swift
//  PunkCervejaria
//
//  Created by Diego Bueno on 16/07/21.
//  Copyright © 2021 Diego Bueno. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {
    //    Segunda tela (Detalhe):
    //    Imagem da cerveja (image_url). Exibir nome (name) Tagline (tagline) Teor alcoólico (abv)
    //    Escala de amargor (ibu) Descrição (description)
    let beer: BeerDetails
    let viewModel: ViewModel
    
    private lazy var myImageView: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private lazy var nameLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var taglineLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var teorLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var amargorLabel: UILabel = {
        let label = UILabel(frame: .zero)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private lazy var descriptionLabel: UILabel = {
        let text = UILabel(frame: .zero)
        text.translatesAutoresizingMaskIntoConstraints = false
        return text
    }()
    
    private lazy var descriptionTextView: UITextView = {
        let text = UITextView(frame: .zero)
        text.translatesAutoresizingMaskIntoConstraints = false
        return text
    }()
    
    init(beer: BeerDetails, viewModel: ViewModel) {
        self.beer = beer
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.frame = UIScreen.main.bounds
        self.view.backgroundColor = .white
        
        view.addSubview(myImageView)
        view.addSubview(nameLabel)
        view.addSubview(taglineLabel)
        view.addSubview(teorLabel)
        view.addSubview(amargorLabel)
        view.addSubview(descriptionLabel)
        view.addSubview(descriptionTextView)
        
        setConfig()
        setConstraints()
    }

    func setConfig() {
        myImageView.contentMode = .scaleAspectFit
        myImageView.clipsToBounds = true
        myImageView.image = viewModel.getImage(strImage: beer.image_url!)
        
        var text = ""
        if let name = beer.name {
            text += name
        }
        nameLabel.text = text
        nameLabel.textAlignment = .center
        nameLabel.font = .systemFont(ofSize: 24, weight: .bold)
        nameLabel.numberOfLines = 3
        nameLabel.textColor = .black
        
        text = ""
        if let tagline = beer.tagline {
            text += tagline
        }
        taglineLabel.text = text
        taglineLabel.textAlignment = .center
        taglineLabel.numberOfLines = 2
        taglineLabel.font = .systemFont(ofSize: 13, weight: .regular)
        taglineLabel.textColor = .black
        
        text = "Alcohol content: "
        if let abv = beer.abv {
            text += "\(abv)"
        }
        teorLabel.text = text
        teorLabel.textAlignment = .left
        teorLabel.font = .systemFont(ofSize: 13, weight: .bold)
        teorLabel.textColor = .black
        
        text = "Bitterness scale: "
        if let amargor = beer.ibu {
            text += "\(amargor)"
        }
        amargorLabel.text = text
        amargorLabel.textAlignment = .left
        amargorLabel.font = .systemFont(ofSize: 13, weight: .bold)
        amargorLabel.textColor = .black
                
        descriptionLabel.text = "Description:"
        descriptionLabel.textAlignment = .center
        descriptionLabel.font = .systemFont(ofSize: 17, weight: .medium)
                
        text = ""
        if let desc = beer.description {
            text += desc
        }
        descriptionTextView.text = text
        descriptionTextView.textAlignment = .center
        descriptionTextView.font = .systemFont(ofSize: 17, weight: .regular)
        descriptionTextView.textColor = .black
        descriptionTextView.isEditable = false

    }
    func setConstraints() {
        
        myImageView.centerYAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.centerYAnchor, constant: -185).isActive = true
        myImageView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 10).isActive = true
        myImageView.heightAnchor.constraint(equalToConstant: 200).isActive = true
        myImageView.widthAnchor.constraint(equalToConstant: 100).isActive = true
        
        nameLabel.leadingAnchor.constraint(equalTo: myImageView.trailingAnchor, constant: 10).isActive = true
        nameLabel.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 20).isActive = true
        nameLabel.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: -10).isActive = true
        nameLabel.heightAnchor.constraint(equalToConstant: 75).isActive = true

        taglineLabel.leadingAnchor.constraint(equalTo: myImageView.trailingAnchor, constant: 10).isActive = true
        taglineLabel.topAnchor.constraint(equalTo: nameLabel.topAnchor, constant: 60).isActive = true
        taglineLabel.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: -10).isActive = true
        taglineLabel.heightAnchor.constraint(equalToConstant: 35).isActive = true
        
        teorLabel.leadingAnchor.constraint(equalTo: myImageView.trailingAnchor, constant: 10).isActive = true
        teorLabel.topAnchor.constraint(equalTo: taglineLabel.topAnchor, constant: 40).isActive = true
        teorLabel.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: -10).isActive = true
        teorLabel.heightAnchor.constraint(equalToConstant: 35).isActive = true
        
        amargorLabel.leadingAnchor.constraint(equalTo: myImageView.trailingAnchor, constant: 10).isActive = true
        amargorLabel.topAnchor.constraint(equalTo: teorLabel.topAnchor, constant: 20).isActive = true
        amargorLabel.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: -10).isActive = true
        amargorLabel.heightAnchor.constraint(equalToConstant: 35).isActive = true
        
        descriptionLabel.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 10).isActive = true
        descriptionLabel.topAnchor.constraint(equalTo: myImageView.bottomAnchor, constant: 20).isActive = true
        descriptionLabel.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: -10).isActive = true
        descriptionLabel.heightAnchor.constraint(equalToConstant: 35).isActive = true
                
        descriptionTextView.leadingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.leadingAnchor, constant: 10).isActive = true
        descriptionTextView.topAnchor.constraint(equalTo: descriptionLabel.topAnchor, constant: 50).isActive = true
        descriptionTextView.trailingAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.trailingAnchor, constant: -10).isActive = true
        descriptionTextView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: -20).isActive = true
    }
}
