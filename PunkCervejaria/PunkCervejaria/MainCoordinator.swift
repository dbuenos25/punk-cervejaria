//
//  MainCoordinator.swift
//  PunkCervejaria
//
//  Created by Diego Bueno on 16/07/21.
//  Copyright © 2021 Diego Bueno. All rights reserved.
//

import UIKit

class MainCoordinator: Coordinator {
    var navigationController: UINavigationController
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    func start() {
        let viewModel = ViewModel(coordinator: self)
        let vc = ViewController(coordinator: self, viewModel: viewModel)
        navigationController.pushViewController(vc, animated: true)
    }
    
    func beerDetail(beer: BeerDetails, viewModel: ViewModel){
        let vc = DetailViewController(beer: beer, viewModel: viewModel)
        navigationController.pushViewController(vc, animated: true)
    }
    
}
