//
//  Coordinator.swift
//  PunkCervejaria
//
//  Created by Diego Bueno on 16/07/21.
//  Copyright © 2021 Diego Bueno. All rights reserved.
//

import UIKit

protocol Coordinator {
    var navigationController: UINavigationController { get set }
    func start()
}
