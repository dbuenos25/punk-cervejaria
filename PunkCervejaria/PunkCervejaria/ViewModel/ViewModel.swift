//
//  ViewModel.swift
//  PunkCervejaria
//
//  Created by Diego Bueno on 15/07/21.
//  Copyright © 2021 Diego Bueno. All rights reserved.
//

import Foundation
import UIKit

class ViewModel {
    
    private let coordinator: MainCoordinator
    
    init(coordinator: MainCoordinator) {
        self.coordinator = coordinator
    }
    
    func fetchBeers(url: URL, completion: @escaping([BeerDetails])->()) -> () {
        
        let task = URLSession.shared.dataTask(with: URLRequest(url: url)) { (data, response, error) in
            if error != nil {
                return
            }
            do {
                let result: [BeerDetails] = try JSONDecoder().decode([BeerDetails].self, from: data!)
                completion(result)
                
            }catch let error {
                print(error.localizedDescription)
                return
            }
       }
       task.resume()
    }
    
    func getImage(strImage: String) -> UIImage? {
        var myImage : UIImage?
        
        if let imagemURL = URL(string: strImage) {
            if let imageData = try? Data(contentsOf: imagemURL){
                myImage = UIImage(data: imageData)
            } else if let image = Bundle.main.path(forResource: "default", ofType: "png") {
                myImage = UIImage(contentsOfFile: image)
            }
        }else if let image = Bundle.main.path(forResource: "default", ofType: "png") {
            myImage = UIImage(contentsOfFile: image)
        }
        
        return myImage
    }
}
